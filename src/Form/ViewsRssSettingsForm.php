<?php

namespace Drupal\vrssfa\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * @file
 * Contains \Drupal\vrssfa\Form\ViewsRssSettingsForm.
 */
/**
 * {@inheritdoc}
 */
class ViewsRssSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'views_rss_authentication.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'views_rss_authentication_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('views_rss_authentication.settings');
    $options = [
      'basic_auth' => $this->t('Basic Auth'),
      'oauth2' => $this->t('Oauth2'),
    ];
    $form['authentication_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Select Authentication Type'),
      '#options' => $options,
      '#default_value' => $config->get('authentication_type'),
    ];
    $form['view_rss_feed_url'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Page URL(s)'),
      '#default_value' => $config->get('view_rss_feed_url'),
      '#description' => $this->t('Specify pages by using their paths. Enter one path per line. An example path is "/article-rss-feed.xml" for every user page.'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    parent::submitForm($form, $form_state);
    // Retrieve the configuration.
    $this->configFactory->getEditable('views_rss_authentication.settings')
    // Set the submitted configuration setting.
      ->set('view_rss_feed_url', $form_state->getValue('view_rss_feed_url'))
      ->set('authentication_type', $form_state->getValue('authentication_type'))
      ->save();
    $this->messenger()->addMessage($this->t("The configuration options have been saved."));
  }

}
