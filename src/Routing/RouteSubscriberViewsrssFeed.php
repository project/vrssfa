<?php

namespace Drupal\vrssfa\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Routing\RouteProviderInterface;

/**
 * Listens to the dynamic route events.
 */
class RouteSubscriberViewsrssFeed extends RouteSubscriberBase {

  /**
   * Configuration Factory.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;
  /**
   * The route provider.
   *
   * @var \Drupal\Core\Routing\RouteProviderInterface
   */
  protected $routeProvider;

  /**
   * Constructor.
   */
  public function __construct(ConfigFactory $configFactory, RouteProviderInterface $route_provider) {
    $this->configFactory = $configFactory;
    $this->routeProvider = $route_provider;
  }

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {

    // Get RSS Feed URLs list.
    $config_data = $this->configFactory->get('views_rss_authentication.settings')->get('view_rss_feed_url');
    // Get selected authentication type.
    $authentication_type = $this->configFactory->get('views_rss_authentication.settings')->get('authentication_type');

    if ($config_data) {
      $urls = explode(PHP_EOL, $config_data);
      foreach ($urls as $url) {
        $path = trim($url);
        // Get route name by its path.
        $found_routes = $this->routeProvider->getRoutesByPattern($path);
        $route_names = [];
        foreach ($found_routes as $route_name => $route_object) {
          $route_names[] = $route_name;
        }
        if ($route = $collection->get($route_names[0])) {
          $route->setOption('_auth', [$authentication_type]);
          $route->setRequirement('_user_is_logged_in', 'TRUE');
          $collection->add(($route_names[0]), $route);
        }
      }
    }
  }

}
